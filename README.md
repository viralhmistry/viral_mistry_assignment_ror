## Requirements

- Ruby 2.6.5

## Dependencies, Test and Run

- Dependencies: `bundle install`

## Example Of API:

- POST '/api/robot/0/orders'

  Input:
    { commands: [ 'PLACE 0,0,NORTH', 'MOVE' ] }

	Output:
    { location: [0,1,'NORTH'] }
