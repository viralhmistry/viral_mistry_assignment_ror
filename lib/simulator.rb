require 'board'
require 'toyrobot'

class Simulator

  def initialize
    @board = Board.new 4, 4
    @robot = Toyrobot.new @board
  end

  def execute(command)
    begin
      @robot.eval(command)
    rescue Exception => e
      e.message
    end
  end

end
