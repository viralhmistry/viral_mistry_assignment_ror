require 'simulator'

class RobotOrdersController < ApplicationController
  before_action :valid_argument

  def move
    @commands = params['commands']
    simulator = Simulator.new

    @commands.each do |command|
      simulator.execute(command)
    end
    output = simulator.execute('report')
    output = output.to_s.split(',') if output.present?

    render json: { location: output }, status: :ok
  end

  private

  def valid_argument
    return if params['commands'].present? && params['commands'].is_a?(Array)

    render json: { message: 'Invalid input' }, status: :unprocessable_entity
  end
end
